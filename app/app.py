from typing import List
import databases
import sqlalchemy
from sqlalchemy import Table, Column, String, DateTime
from fastapi import FastAPI, status
from fastapi.middleware.cors import CORSMiddleware
from fastapi.responses import JSONResponse
from fastapi.encoders import jsonable_encoder
from pydantic import BaseModel
import os
import urllib
from sqlalchemy.dialects.postgresql import UUID as UUIDtype
from uuid import UUID
from typing import Optional
import uuid


# Lets pick up the db from the env variables
DATABASE_URL = os.environ.get('DATABASE_URL', '')

database = databases.Database(DATABASE_URL)

metadata = sqlalchemy.MetaData()

def generate_uuid():
    return str(uuid.uuid4())

designs = Table(
    "designs",
    metadata,
    Column("id", UUIDtype, primary_key=True, default=uuid.uuid4),
    Column("name", String),
)

engine = sqlalchemy.create_engine(
    DATABASE_URL
)
metadata.create_all(engine)


class DesignIn(BaseModel):
    name: str

    class Config:
        orm_mode = True
        arbitrary_types_allowed = True
        schema_extra = {
            "example": {
                "name": "My design",
            }
        }


class Design(BaseModel):
    id: uuid.UUID
    name: str

    class Config:
        orm_mode = False
#         orm_mode = True
        arbitrary_types_allowed = True
        schema_extra = {
            "example": {
                "id": "0657b5a7-7a86-4204-ad2b-a8e91dc760dd",
                "name": "My design",
            }
        }


app = FastAPI()
app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
    expose_headers=["X-Total-Count", "Content-Range"]
)

@app.on_event("startup")
async def startup():
    await database.connect()


@app.on_event("shutdown")
async def shutdown():
    await database.disconnect()


# root action
@app.get("/")
async def root():
    return {
        "@context": "/contexts/Entrypoint",
        "@id": "/",
        "@type": "Entrypoint",
        "design": "/designs"
    }


# designs actions
@app.get("/designs/", response_model=List[Design])
async def read_designs():
    query = design.select()
    contentList = await database.fetch_all(query)
    content = jsonable_encoder(contentList)
    return JSONResponse(content=content, headers={"X-Total-Count" : str(len(contentList))})


@app.post("/designs/", response_model=Design, status_code=201)
async def create_design(design: DesignIn):
    uuid = generate_uuid()
    query = designs.insert().values(id=uuid, name=design.name, status=design.status)
    await database.execute(query)
    return {"id": uuid, **design.dict()}


@app.get("/designs/{design_id}", response_model=Design)
async def read_designs(design_id: uuid.UUID):
    query = designs.select().where(designs.c.id == design_id)
    return await database.fetch_one(query)


@app.put("/designs/{design_id}", response_model=Design)
async def update_design(design_id: uuid.UUID, design: DesignIn):
    query = designs.update().where(designs.c.id == design_id).values(name=design.name, status=design.status)
    await database.execute(query)
    return {"id": design_id, **design.dict()}


@app.delete("/designs/{design_id}")
async def delete_designs(design_id: uuid.UUID):
    # todo:should also check if this id is an existing design object, because this will still return the delete message if it doesn't
    query = designs.delete().where(designs.c.id == design_id)
    await database.execute(query)
    return {"message": f"Design with id: {design_id} deleted successfully"}
