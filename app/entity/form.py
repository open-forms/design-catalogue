# import sqlalchemy
# from typing import List
# from pydantic import BaseModel
# from sqlalchemy.dialects.postgresql import UUID
# import uuid
#
# from app.db.db import metadata
# forms = sqlalchemy.Table(
#     "forms",
#     metadata,
#     sqlalchemy.Column("id", sqlalchemy.Integer, primary_key=True),
# #     sqlalchemy.Column("id", UUID(as_uuid=True), primary_key=True, default=uuid.uuid4),
#     sqlalchemy.Column("status", sqlalchemy.String),
#     sqlalchemy.Column("name", sqlalchemy.String),
#     sqlalchemy.Column("form", sqlalchemy.String),
#     sqlalchemy.Column("config", sqlalchemy.String),
# )
#
# class FormIn(BaseModel):
#     status: str
#     name: str
#     form: str
#     config: str
#
#     class Config:
#         orm_mode = True
#
#
# class Form(BaseModel):
#     id: int
#     status: str
#     name: str
#     form: str
#     config: str
#
#     class Config:
#         orm_mode = True
#
# # forms actions
#
# @app.get("/forms/", response_model=List[Form])
# async def read_forms():
#     query = forms.select()
#     return await database.fetch_all(query)
#
#
# @app.post("/forms/", response_model=Form, status_code=201)
# async def create_form(form: FormIn):
#     query = forms.insert().values(status=form.status, name=form.name, form=form.form, config=form.config)
#     last_record_id = await database.execute(query)
#     return {**form.dict(), "id": last_record_id}
#
#
# @app.get("/forms/{form_id}", response_model=Form)
# async def read_forms(form_id: int):
#     query = forms.select().where(forms.c.id == form_id)
#     return await database.fetch_one(query)
#
#
# @app.put("/forms/{form_id}", response_model=Form)
# async def update_form(form_id: int, form: FormIn):
#     query = forms.update().where(forms.c.id == form_id).values(status=form.status, name=form.name, form=form.form,
#                                                                config=form.config)
#     await database.execute(query)
#     return {**form.dict(), "id": form_id}
#
#
# @app.delete("/forms/{form_id}")
# async def delete_forms(form_id: int):
#     query = forms.delete().where(forms.c.id == form_id)
#     await database.execute(query)
#     return {"message": f"Form with id: {form_id} deleted successfully"}
